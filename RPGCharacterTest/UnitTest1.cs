using RPGCharacter;
using RPGCharacter.Classes;
using RPGCharacter.Enemies;
using RPGCharacter.Equipment;
using RPGCharacter.Inventory;

namespace RPGCharacterTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            CharacterClass myChar = new();

            float expectedDamage = 6;
            float actualDamage = myChar.CalculateDamage();

            Assert.AreEqual(expectedDamage, actualDamage, 0.001, "'t is wrong!");
        }
        /// I cannot get the tests to run because my app uses JSON databases to create items. 
        /// I have no idea how to get it to find the files in the proper location.
    }
}