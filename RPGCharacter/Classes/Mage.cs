﻿using RPGCharacter.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Inventory;

namespace RPGCharacter.Classes
{
    public class Mage : CharacterClass
    {
        /// <summary>
        /// Generates a Class with the stats of a Mage
        /// </summary>
        public Mage() : base()
        {
            primaryStat = Stat.intelligence;

            stats.Add(Stat.strength, 1);
            stats.Add(Stat.dexterity, 1);
            stats.Add(Stat.intelligence, 8);
            health = 75;

            weaponSkills.Add(WeaponType.Staff);
            weaponSkills.Add(WeaponType.Wand);

            armorSkills.Add(ArmorType.Cloth);

        }
    }
}
