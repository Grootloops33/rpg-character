﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacter.Classes
{
    public class Ranger : CharacterClass
    {
        /// <summary>
        /// Generates a Class with the stats of a Ranger
        /// </summary>
        public Ranger() : base()
        {
            primaryStat = Stat.dexterity;

            stats.Add(Stat.strength, 1);
            stats.Add(Stat.dexterity, 7);
            stats.Add(Stat.intelligence, 1);
            health = 90;

            weaponSkills.Add(Equipment.WeaponType.Bow);

            armorSkills.Add(Equipment.ArmorType.Leather);
            armorSkills.Add(Equipment.ArmorType.Mail);
        }
    }
}
