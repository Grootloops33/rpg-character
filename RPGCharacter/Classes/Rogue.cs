﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacter.Classes
{
    public class Rogue : CharacterClass
    {
        /// <summary>
        /// Generates a Class with the stats of a Rogue
        /// </summary>
        public Rogue(): base()
        {
            primaryStat = Stat.dexterity;

            stats.Add(Stat.strength, 2);
            stats.Add(Stat.dexterity, 6);
            stats.Add(Stat.intelligence, 1);
            health = 110;

            weaponSkills.Add(Equipment.WeaponType.Dagger);
            weaponSkills.Add(Equipment.WeaponType.Sword);

            armorSkills.Add(Equipment.ArmorType.Leather);
            armorSkills.Add(Equipment.ArmorType.Mail);
        }
    }
}
