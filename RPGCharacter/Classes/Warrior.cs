﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Equipment;

namespace RPGCharacter.Classes
{
    public class Warrior : CharacterClass
    {
        /// <summary>
        /// Generates a Class with the stats of a Warrior
        /// </summary>
        public Warrior() : base()
        {
            primaryStat = Stat.strength;

            stats.Add(Stat.strength, 5);
            stats.Add(Stat.dexterity, 2);
            stats.Add(Stat.intelligence, 1);
            health = 125;

            weaponSkills.Add(WeaponType.Axe);
            weaponSkills.Add(WeaponType.Hammer);
            weaponSkills.Add(WeaponType.Sword);

            armorSkills.Add(ArmorType.Mail);
            armorSkills.Add(ArmorType.Plate);

        }
    }
}
