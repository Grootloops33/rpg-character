﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RPGCharacter.Inventory;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacter.Equipment
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    class Armor : Item
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ArmorType armorType;
        [JsonProperty("armorValue")]
        public int armorValue;
        [JsonProperty("armorToughness")]
        public int toughness;
        [JsonProperty("requiredLevel")]
        public int requiredLevel;
    }
}
