﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using RPGCharacter.Inventory;

namespace RPGCharacter.Equipment
{
    public enum WeaponType
    {
        Axe, //ignores some armor 
        Bow, //neutral speed, neutral damage
        Dagger, //high speed, low damage
        Hammer, //high damage, low speed
        Staff, //big amount intelligence but no ranged attack
        Sword, //neutral speed, neutral damage
        Wand //small amount of int with ranged attack
    }

    public class Weapon : Item
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public WeaponType type;
        [JsonProperty("requiredLevel")]
        public int requiredLevel;
        [JsonProperty("damage")]
        public float damage;
        [JsonProperty("speed")]
        public float speed;

    }
}
