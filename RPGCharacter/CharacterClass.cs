﻿using System;
using System.Collections.Generic;
using System.Text;
using RPGCharacter.Equipment;
using RPGCharacter.Inventory;
using RPGCharacter.Classes;

namespace RPGCharacter
{
    public class CharacterClass
    {
        public enum Stat
        {
            strength,
            dexterity,
            intelligence
        }

        public enum CLASS
        {
            mage,
            ranger,
            rogue,
            warrior
        }

        public List<WeaponType> weaponSkills = new List<WeaponType>();
        public List<ArmorType> armorSkills = new List<ArmorType>();

        public Dictionary<Stat, int> stats = new Dictionary<Stat, int>(); // change string to stat type
        // damage calculation 
        // IncomingDamage = weaponDamage x ( 1 - min(20, max(armorValue / 5, armorValue - (4 * weaponDamage / (toughness+8)))) / 25) 


        public Dictionary<Slot, Item> equipment = new Dictionary<Slot, Item>();
        public Inventory.Inventory inventory = new Inventory.Inventory();

        public string characterName;

        public float health;
        public float dodge;
        public int armorValue;

        public float damage;

        public Stat primaryStat;
        public CLASS selectedClass;

        public byte level = 1;

        public CharacterClass()
        {
            string[] e = Enum.GetNames(typeof(Slot));
            for (int i = 0; i < e.Length; i++)
            {
                equipment.Add(Enum.Parse<Slot>(e[i]), null);
            }
            
        }
        /// <summary>
        /// Level up your character and increase its stats.
        /// </summary>
        public void LevelUp()
        {
            level++;
            CalculateDamage();

            switch (selectedClass)
            {
                case CLASS.mage:
                    {
                        stats[Stat.intelligence] += 5;
                        stats[Stat.strength] += 1;
                        stats[Stat.dexterity] += 1;
                        break;
                    }
                case CLASS.ranger:
                    {
                        stats[Stat.intelligence] += 1;
                        stats[Stat.strength] += 1;
                        stats[Stat.dexterity] += 5;
                        break;
                    }
                case CLASS.rogue:
                    {
                        stats[Stat.intelligence] += 1;
                        stats[Stat.strength] += 1;
                        stats[Stat.dexterity] += 4;
                        break;
                    }
                case CLASS.warrior:
                    {
                        stats[Stat.intelligence] += 1;
                        stats[Stat.strength] += 3;
                        stats[Stat.dexterity] += 2;
                        break;
                    }
            }

            health = health * (stats[Stat.strength] / 10);

            Console.WriteLine($"You have leveled up to {level}! \nYour stats are now: " +
                $"\nStrength: {stats[Stat.strength]}" +
                $"\nDexterity: {stats[Stat.dexterity]}" +
                $"\nIntelligence: {stats[Stat.intelligence]}");

        }
        /// <summary>
        /// Recalculate the damage your character can deal
        /// </summary>
        /// <returns>this returns a float</returns>
        public new float CalculateDamage()
        {
            Weapon w = (Weapon)equipment[Slot.Weapon];
            if(w != null)
            {
                return w.damage * (1 + stats[primaryStat]) / w.speed;
            }
            else
            {
                return 1 * (1 + 5);
            }
        }

        public new float TakeDamage(float damage)
        {
            List<Armor> equippedArmor = new();
            float damageReduc = 0;
            float toughness = 0;
            foreach (var e in equipment.Values)
            {
                if(e.slot != Slot.Weapon)
                {
                    equippedArmor.Add((Armor)e);
                }
            }

            foreach (var e in equippedArmor)
            {
                damageReduc += e.armorValue;
                toughness += e.toughness;
            }
            damage *= 1 - Math.Min(20, Math.Max(damageReduc / 5, damageReduc - ((4 * damage) / toughness + 8)));

            return damage;
        }
    }
}
