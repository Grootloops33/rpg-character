﻿using System;
using System.Collections.Generic;
using RPGCharacter.Classes;
using RPGCharacter.Equipment;
using RPGCharacter.Inventory;
using RPGCharacter.Enemies;

namespace RPGCharacter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please choose a class");
            Console.WriteLine("Available Classes: \n Rogue \n Warrior \n Mage \n Ranger");
            string s = Console.ReadLine();

            CharacterClass myClass = null;
            switch (s.ToLower())
            {
                case "mage":
                {
                    myClass = new Mage();
                    break;
                }
                case "rogue":
                {
                    myClass = new Rogue();
                    break;
                }
                case "ranger":
                {
                    myClass = new Ranger();
                    break;
                }
                case "warrior":
                {
                    myClass = new Warrior();
                    break;
                }
                default:
                {
                    Console.WriteLine("You did not select a valid class");
                    return;
                }
            }

            Console.WriteLine("\nWhat is your name?");
            myClass.characterName = Console.ReadLine();

            Console.WriteLine($"\nYou're a {s}! Your health is {myClass.health} and your name is {myClass.characterName}");
            Console.WriteLine($"Your attributes are: \nStrength: {myClass.stats[CharacterClass.Stat.strength]} " +
                $"\nDexterity: {myClass.stats[CharacterClass.Stat.dexterity]} " +
                $"\nIntelligence: {myClass.stats[CharacterClass.Stat.intelligence]}");

            while(true)
            {
                ListOptions();
            }

            /// <summary>
            /// Writes a list of all available options in the console.
            /// Can Throw InvalidArmorException if armor attempted to equip does not match required stats.
            /// </summary>
            void ListOptions()
            {
                Console.WriteLine("\nWhat would you like to do?");
                Console.WriteLine("Options: " +
                    "\n{1} Fight a boss " +
                    "\n{2} Eat an EXP cube " +
                    "\n{3} Show me my Inventory" +
                    "\n{4} Manage Equipment" +
                    "\n{5} Character Sheet");
                string opt = Console.ReadLine();
                switch (opt)
                {
                    case "1":
                        {
                            myClass.inventory.AddToInventory();
                            break;
                        }
                    case "2":
                        {
                            myClass.LevelUp();
                            break;
                        }
                    case "3":
                        {
                            foreach (var item in myClass.inventory.inv)
                            {
                                Inventory.Inventory.WriteItem(item);
                            }
                            break;
                        }
                    case "4":
                        {
                            if(myClass.inventory.inv.Count <= 0)
                            {
                                Console.Write("You don't have any gear yet!");
                            }
                            else
                            {
                                Console.WriteLine("\nPlease select a slot");

                                Console.WriteLine("======Currently Equipped======");
                                int i = 0;
                                foreach (var e in myClass.equipment)
                                {
                                    i++;
                                    if(e.Value != null)
                                    {
                                        Console.WriteLine($"[{i}]{e.Key}: {e.Value.name}");
                                    }
                                    else
                                    {
                                        Console.WriteLine($"[{i}]{e.Key} [EMPTY] ");
                                    }
                                }
                                Console.WriteLine("==============================");
                                string re = Console.ReadLine();

                                List<Item> itemOp_List = new List<Item>();
                                Slot selectedSlot = Slot.Head;
                                switch (re)
                                {
                                    case "1":
                                        {
                                            Console.WriteLine("======Available Helmets=======");
                                            selectedSlot = Slot.Head;
                                            foreach (var item in myClass.inventory.inv)
                                            {
                                                if(item.slot == Slot.Head)
                                                {
                                                    itemOp_List.Add(item);
                                                }
                                            }
                                            break;
                                        }
                                    case "2":
                                        {
                                            Console.WriteLine("======Available Chestplates===");
                                            selectedSlot = Slot.Body;
                                            foreach (var item in myClass.inventory.inv)
                                            {
                                                if (item.slot == Slot.Body)
                                                {
                                                    itemOp_List.Add(item);
                                                }
                                            }
                                            break;
                                        }
                                    case "3":
                                        {
                                            Console.WriteLine("======Available Leggings======");
                                            selectedSlot = Slot.Legs;
                                            foreach (var item in myClass.inventory.inv)
                                            {
                                                if (item.slot == Slot.Legs)
                                                {
                                                    itemOp_List.Add(item);
                                                }
                                            }
                                            break;
                                        }
                                    case "4":
                                        {
                                            Console.WriteLine("======Available Weapons=======");
                                            selectedSlot = Slot.Weapon;
                                            foreach (var item in myClass.inventory.inv)
                                            {
                                                if (item.slot == Slot.Weapon)
                                                {
                                                    itemOp_List.Add(item);
                                                }
                                            }
                                            break;
                                        }
                                    default:
                                        {
                                            return;
                                        }
                                }

                                Item[] itemOP_Arr = itemOp_List.ToArray();
                                for (int j = 0; j < itemOP_Arr.Length; j++)
                                {
                                    Console.WriteLine($"{j}: {itemOP_Arr[j].name}");
                                }

                                Console.WriteLine("\nPlease select an item to equip");
                                int x = int.Parse(Console.ReadLine());
                                // Armor Equipping
                                if (itemOP_Arr[x].slot != Slot.Weapon)
                                {
                                    Armor a = (Armor)itemOP_Arr[x];
                                    if (myClass.armorSkills.Contains(a.armorType) || myClass.level >= a.requiredLevel)
                                    {
                                        myClass.equipment[selectedSlot] = itemOP_Arr[x];
                                    }
                                    else if(!myClass.armorSkills.Contains(a.armorType))
                                    {
                                        throw new InvalidArmorException("You cannot equip that piece due to lack of Armor skill!");
                                        break; 
                                    }
                                    else if (myClass.level < a.requiredLevel)
                                    {
                                        throw new InvalidArmorException("You are not the right level to equip this armor!");
                                        break;
                                    }
                                }
                                //Weapon Equipping
                                if (itemOP_Arr[x].slot == Slot.Weapon)
                                {
                                    Weapon w = (Weapon)itemOP_Arr[x];
                                    if (myClass.weaponSkills.Contains(w.type) || myClass.level >= w.requiredLevel)
                                    {

                                    }
                                }


                            }
                            break;
                        }
                    case "5":
                        {
                            foreach (var stat in myClass.stats)
                            {
                                Console.WriteLine($"{stat.Key}: {stat.Value}\n");
                            }
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid Option!");
                            return;
                        }
                }
            }
        }
    }

    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() : base() { }
        public InvalidArmorException(string message) : base(message) { }
        public InvalidArmorException(string message, Exception inner) : base(message, inner) { }

    }
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() : base() { }
        public InvalidWeaponException(string message) : base(message) { }
        public InvalidWeaponException(string message, Exception inner) : base(message, inner) { }

    }
}
