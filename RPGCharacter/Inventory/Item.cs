﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Equipment;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RPGCharacter.Inventory
{
    public class Item
    {
        [JsonProperty("id")]
        public int id;
        [JsonProperty("name")]
        public string name;
        [JsonConverter(typeof(StringEnumConverter))]
        public Slot slot;
    }
}
