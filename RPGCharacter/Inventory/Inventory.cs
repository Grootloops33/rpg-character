﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using RPGCharacter.Equipment;

namespace RPGCharacter.Inventory
{
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon
    }


    public class Inventory
    {
        public Dictionary<string, Item> items = new Dictionary<string, Item>();
        public List<Item> inv = new List<Item>();
        /// <summary>
        /// Instantiates a new Inventory
        /// </summary>
        public Inventory()
        {
            Dictionary<string, Weapon> weapons = JsonConvert.DeserializeObject<Dictionary<string, Weapon>>(File.ReadAllText(@".\Inventory\WeaponDatabase.json"));
            Dictionary<string, Armor> armor = JsonConvert.DeserializeObject<Dictionary<string, Armor>>(File.ReadAllText(@".\Inventory\ArmorDatabase.json"));

            foreach (var w in weapons)
            {
                items.Add(w.Key, w.Value);
            }
            foreach (var a in armor)
            {
                items.Add(a.Key, a.Value);
            }

            foreach (var item in items)
            {
                WriteItem(item.Value);
            }
        }

        /// <summary>
        /// Add a random item to the inventory.
        /// </summary>
        public void AddToInventory()
        {
            Random r = new Random();
            int x = r.Next(0, items.Values.Count);
            Item i = items.Values.ToArray()[x];

            inv.Add(i);
            WriteItem(i);
        }

        /// <summary>
        /// Display an Item Class' information in the console
        /// </summary>
        /// <param name="item">The Item to be written</param>
        public static void WriteItem(Item item)
        {
            if (item.slot == Slot.Weapon)
            {
                Weapon w = (Weapon)item;
                Console.WriteLine($"Weapon {w.name} ID: {w.id} TYPE: {w.type}");
            }

            if (item.slot != Slot.Weapon)
            {
                Armor w = (Armor)item;
                Console.WriteLine($"Armor {w.name} ID: {w.id} ARMOR: {w.armorValue} TOUGH: {w.toughness} SLOT: {w.slot} TYPE: {w.armorType}");
            }
        }
    }
}
